package fr.upem.java.tcp.matou;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;

public class AcceptedPrivateMessageReader implements Reader {
    private enum State {DONE, WAITINGLOGIN, WAITINGADRESS, ERROR}

    private final ByteBuffer bb;
    private State state = State.WAITINGLOGIN;
    private StringData login;
    private InetSocketAddress clientAdress;

    AcceptedPrivateMessageReader(ByteBuffer bb) {
        this.bb = bb;
    }

    /**
     * Process the buffer to get information.
     * login and InetSocketAdress's server of requested user
     * @return State after process
     */
    @Override
    public ProcessStatus process() {
        StringReader sr = new StringReader(bb);
        InternetProtocolReader ipReader = new InternetProtocolReader(bb);

        switch (state) {
            case DONE:
                throw new IllegalStateException();

            case ERROR:
                throw new IllegalStateException();

            case WAITINGLOGIN:
                switch (sr.process()) {
                    case DONE:
                        login = (StringData) sr.get();
                        state = State.WAITINGADRESS;
                        sr.reset();
                        break;

                    case REFILL:
                        return ProcessStatus.REFILL;

                    case ERROR:
                        System.out.println("Error WaitingLogin");
                        return ProcessStatus.ERROR;
                }

            case WAITINGADRESS:
                switch (ipReader.process()) {
                    case DONE:
                        clientAdress = (InetSocketAddress) ipReader.get();
                        state = State.DONE;
                        sr.reset();
                        return ProcessStatus.DONE;

                    case REFILL:
                        return ProcessStatus.REFILL;

                    case ERROR:

                        System.out.println("Error WaitingAdress");
                        return ProcessStatus.ERROR;
                }
        }
        return ProcessStatus.ERROR;
    }

    /**
     * get the information from reader
     * @return {@code PrivateMessageConnectionData} if process DONE
     */
    @Override
    public Object get() {
        if (state != State.DONE) {
            throw new IllegalStateException();
        }
        return new PrivateMessageConnectionData(login, clientAdress);
    }

    /**
     * reset the reader for future usage
     */
    @Override
    public void reset() {
        state = State.WAITINGLOGIN;
    }
}
