package fr.upem.java.tcp.matou;

import java.net.InetSocketAddress;

class PrivateMessageConnectionData {
    private final StringData login;
    private final InetSocketAddress clientAddress;

    PrivateMessageConnectionData(StringData login, InetSocketAddress clientAddress) {
        this.login = login;
        this.clientAddress = clientAddress;
    }

    StringData getLogin() {
        return login;
    }

    InetSocketAddress getClientAddress() {
        return clientAddress;
    }
}
