package fr.upem.java.tcp.matou;

import java.nio.ByteBuffer;

public class FileRequestReader implements Reader {
    private enum State {DONE, WAITINGFILENAME, WAITINGSIZE, ERROR}
    private final ByteBuffer bb;
    private StringData filename;
    private long filesize;
    private State state = State.WAITINGFILENAME;
    private final StringReader stringReader;
    private final LongReader longReader;

    FileRequestReader(ByteBuffer bb) {
        this.bb = bb;
        this.stringReader = new StringReader(bb);
        this.longReader = new LongReader(bb);
    }

    /**
     * Process the buffer to get information.
     * filename and filesize of requested file
     * @return state after process
     */
    @Override
    public ProcessStatus process() {
        if (state==State.DONE || state==State.ERROR) {
            throw new IllegalStateException();
        }
        switch (state) {
            case WAITINGFILENAME:
                switch (stringReader.process()) {
                    case DONE:
                        filename = (StringData) stringReader.get();
                        stringReader.reset();
                        state = State.WAITINGSIZE;
                        break;
                    case REFILL:
                        return ProcessStatus.REFILL;
                    case ERROR:
                        return ProcessStatus.ERROR;
                }

            case WAITINGSIZE:
                switch (longReader.process()) {
                    case DONE:
                        filesize = (Long) longReader.get();
                        longReader.reset();
                        state = State.DONE;
                        return ProcessStatus.DONE;
                    case REFILL:
                        return ProcessStatus.REFILL;
                    case ERROR:
                        return ProcessStatus.ERROR;
                }
                break;
        }
        return ProcessStatus.ERROR;
    }

    @Override
    public Object get() {
        if (state!=State.DONE) {
            throw new IllegalStateException();
        }
        return new FileRequestData(filename, filesize);
    }

    @Override
    public void reset() {
        state = State.WAITINGFILENAME;
    }
}
