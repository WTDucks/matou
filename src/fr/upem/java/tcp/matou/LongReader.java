package fr.upem.java.tcp.matou;

import java.nio.ByteBuffer;

public class LongReader implements Reader {
    private enum State {DONE,WAITING,ERROR}

    private final ByteBuffer bb;
    private State state = State.WAITING;
    private long value;

    LongReader(ByteBuffer bb) {
        this.bb = bb;
    }

    /**
     * Process the buffer to get information.
     * @return state after process
     */
    @Override
    public ProcessStatus process() {
        if (state==State.DONE || state==State.ERROR) {
            throw new IllegalStateException();
        }
        bb.flip();
        try {
            if (bb.remaining() >= Long.BYTES) {
                value = bb.getLong();
                state = State.DONE;
                return ProcessStatus.DONE;
            } else {
                return ProcessStatus.REFILL;
            }
        } finally {
            bb.compact();
        }
    }

    /**
     * get reader information
     * @return {@code long} value
     */
    @Override
    public Object get() {
        if (state != State.DONE) {
            throw new IllegalStateException();
        }
        return value;
    }

    /**
     * reset reader
     */
    @Override
    public void reset() {
        state = State.WAITING;
    }
}
