package fr.upem.java.tcp.matou;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;

public class InternetProtocolReader implements Reader {
    private enum State {DONE, WAITINGIP, WAITINGPORT, ERROR}

    private final ByteBuffer bb;
    private State state = State.WAITINGIP;
    private String IPAdress;
    private int port;
    private final IntReader intReader;
    private final StringReader stringReader;

    InternetProtocolReader(ByteBuffer bb) {
        this.bb = bb;
        intReader = new IntReader(bb);
        stringReader = new StringReader(bb);
    }

    /**
     * Process the buffer to get information.
     * IP Adress and port
     * @return state after process
     */
    @Override
    public ProcessStatus process() {
        if (state==State.DONE || state==State.ERROR) {
            throw new IllegalStateException();
        }


        switch (state) {
            case WAITINGIP:
                switch (stringReader.process()) {
                    case DONE:
                        IPAdress = ((StringData) stringReader.get()).getStr();
                        stringReader.reset();
                        state = State.WAITINGPORT;
                        break;

                    case REFILL:
                        return ProcessStatus.REFILL;

                    case ERROR:
                        return ProcessStatus.ERROR;
                }

            case WAITINGPORT:
                switch (intReader.process()) {
                    case DONE:
                        port = (int) intReader.get();
                        intReader.reset();
                        state = State.DONE;
                        return ProcessStatus.DONE;

                    case REFILL:
                        return ProcessStatus.REFILL;

                    case ERROR:
                        return ProcessStatus.ERROR;
                }
        }
        return ProcessStatus.ERROR;
    }

    @Override
    public Object get() {
        if (state!=State.DONE) {
            throw new IllegalStateException();
        }
        return new InetSocketAddress(IPAdress, port);
    }

    @Override
    public void reset() {
        state = State.WAITINGIP;
    }
}
