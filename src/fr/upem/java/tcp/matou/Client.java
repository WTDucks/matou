package fr.upem.java.tcp.matou;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.*;
import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Logger;

public class Client {
    static private Logger logger = Logger.getLogger(Client.class.getName());

    private final SelectionKey clientKey;
    private final SelectionKey serverKey;
    private final Selector selector;
    private final Set<SelectionKey> selectedKeys;
    private final BlockingQueue<String> inputQueue = new LinkedBlockingQueue<>();
    private final ArrayList<String> authorizedUsers = new ArrayList<>();
    private final HashMap<String, PrivateContext> connectedUsers = new HashMap<>();
    private final String login;

    /**
     * add user to the authorized users list for Private Message
     * @param user username
     */
    void addAuthorizedUser(String user) {
        authorizedUsers.add(user);
    }

    /**
     * remove user for the authorized users list for Private Message
     * @param user username
     */
    void removeAuthorizedUser(String user){
        authorizedUsers.remove(user);
    }

    /**
     * Tell if user is authorized for a private conversation
     * @param user username
     * @return {@code true} if present otherwise {@code false}
     */
    Boolean isUserAuthorized(String user) {
        return authorizedUsers.contains(user);
    }

    /**
     * add client to Connected Users Database
     * @param login username
     * @param privateCtx {@code PrivateContext} of the client
     */
    void addConnectedUser(String login, PrivateContext privateCtx){
        connectedUsers.putIfAbsent(login, privateCtx);
    }

    /**
     * remove client from Connected Users Database
     * @param login username
     */
    void removeConnectedUser(String login){
        connectedUsers.remove(login);
    }

    /**
     * get the {@code PrivateContext} of a client
     * @param login username
     * @return an {@code Optional} with a {@code PrivateContext} value if the specified value
     * is non-{@code null}, otherwise an empty {@code Optional}
     */
    private Optional<PrivateContext> getPrivateContext(String login){
        return Optional.ofNullable(connectedUsers.get(login));
    }

    /**
     * get server information
     * @return {@code InetSocketAdress} information
     * @throws IOException if server disconnected
     */
    InetSocketAddress getServerInfo() throws IOException {
        return (InetSocketAddress) ((ServerSocketChannel) serverKey.channel()).getLocalAddress();
    }

    /**
     * Register a client socketChannel in selector
     * @param client the SocketChannel
     * @param destLogin username of dest client (can be null)
     * @param interestOps interestOps of registered client
     */
    void registerPrivateClient(SocketChannel client, String destLogin, int interestOps) {
        if (interestOps != SelectionKey.OP_CONNECT && interestOps != SelectionKey.OP_READ)
            return;
        try {
            SelectionKey clientKey = client.register(selector, interestOps);
            clientKey.attach(new PrivateClientContext(login, destLogin, clientKey, this));
        } catch (ClosedChannelException e) {
            logger.severe("registerPrivateClient: selector closed");
        }
    }

    public static void main(String[] args) throws IOException {
        if (args.length != 3){
            usage();
            return;
        }
        Client client = new Client(args[0], new InetSocketAddress(args[1], Integer.parseInt(args[2])));
        client.launch();
        System.out.println("See you space cowboy...");
        System.exit(0);
    }

    private Client(String login, InetSocketAddress server) throws IOException {
        this.login = login;
        SocketChannel clientSocketChannel = SocketChannel.open();
        clientSocketChannel.bind(null);
        clientSocketChannel.configureBlocking(false);
        clientSocketChannel.connect(server);
        selector = Selector.open();
        clientKey = clientSocketChannel.register(selector, SelectionKey.OP_CONNECT);
        clientKey.attach(new PublicClientContext(this, clientKey, login));

        ServerSocketChannel privateSCC = ServerSocketChannel.open();
        privateSCC.bind(null);
        privateSCC.configureBlocking(false);
        serverKey = privateSCC.register(selector, SelectionKey.OP_ACCEPT);
        serverKey.attach(new PrivateServerContext(this));
        selectedKeys = selector.selectedKeys();
    }

    /**
     * Runnable that add user input into a Queue
     */
    private void userListenerRun() {
        try(Scanner input = new Scanner(System.in)) {
            while (input.hasNextLine()) {
                String str = input.nextLine();
                inputQueue.put(str);
                selector.wakeup();
            }
        } catch (InterruptedException ignored) {
        }
    }

    /**
     * get a user action from a queue and apply it according user manual
     */
    private void userActions() {
        Boolean displayError = false;
        String str = inputQueue.poll();
        if (str == null) return;
        String[] args = str.split("\\s+");
        String msg;
        PublicClientContext pcc = (PublicClientContext) clientKey.attachment();
        switch (args[0]) {
            case  "/all":
            case "/a":
                try {
                    msg = str.substring(str.indexOf(" ") + 1);
                } catch (IndexOutOfBoundsException e) {
                    displayError = true;
                    break;
                }
                pcc.sendGlobalMessage(msg);
                break;

            case "/exit":
            case "/quit":
                Thread.currentThread().interrupt();
                break;

            case "/askmp":
                if (args.length != 2)
                    displayError = true;
                else
                    pcc.sendPersonalMessageRequest(args[1]);
                break;

            case "/mp":
                if (args.length < 3) {
                    displayError = true;
                } else {
                    try {
                        msg = str.substring(str.indexOf(args[1]) + args[1].length() + 1);
                    } catch (IndexOutOfBoundsException e) {
                        displayError = true;
                        break;
                    }
                    Optional ctx = getPrivateContext(args[1]);
                    if (ctx.isPresent()) {
                        ((PrivateContext) ctx.get()).sendMessage(msg);
                    } else {
                        System.out.println("You need to /askmp before using /mp (see /h for more info)");
                    }
                }
                break;

            case "/acceptmp":
                if (args.length != 2)
                    displayError = true;
                else
                    pcc.sendAcceptedMessageRequest(args[1]);
                break;

            case "/refusemp":
                if (args.length != 2)
                    displayError = true;
                else
                    pcc.sendRefusedMessageRequest(args[1]);
                break;

            case "/askfile":
                if (args.length != 3)
                    displayError = true;
                else {
                    Optional ctx = getPrivateContext(args[1]);
                    if (ctx.isPresent()) {
                        ((PrivateContext) ctx.get()).sendFileSharingRequest(args[2]);
                    } else {
                        System.out.println("You need to /askmp before using /askfile (see /h for more info)");
                    }
                }
                break;

            case "/acceptfile":
                if (args.length != 3)
                    displayError = true;
                else {
                    Optional ctx = getPrivateContext(args[1]);
                    if (ctx.isPresent()) {
                        ((PrivateContext) ctx.get()).sendAcceptedFileSharing(args[2]);
                    } else {
                        System.out.println("You need to /askmp before using /acceptfile (see /h for more info)");
                    }
                }
                break;

            case "/refusefile":
                if (args.length != 3)
                    displayError = true;
                else {
                    Optional ctx = getPrivateContext(args[1]);
                    if (ctx.isPresent()) {
                        ((PrivateContext) ctx.get()).sendRefusedFileSharing(args[2]);
                    } else {
                        System.out.println("You need to /askmp before using /refusefile (see /h for more info)");
                    }
                }
                break;

            case "/h":
            case "/help":
                System.out.println(
                        "Choose an action : \n" +
                        "/all or /a                    - send a global message \n" +
                        "/exit or /quit                - Disconnect from the server \n" +
                        "/askmp pseudo                 - ask for a private discussion with pseudo \n" +
                        "/acceptmp pseudo              - accept the mp request from pseudo \n" +
                        "/refusemp pseudo              - refuse the mp request from pseudo \n" +
                        "/mp pseudo msg                - send a private message msg to pseudo \n" +
                        "/askfile pseudo filename      - ask to pseudo for file sharing \n" +
                        "/acceptfile pseudo filename   - accept the file sharing from pseudo \n" +
                        "/refusefile pseudo filename   - refuse the file sharing \n" +
                        "/help or /h                   - print available commands");
                break;
            default:
                displayError = true;
                break;
        }
        if (displayError)
            System.out.println("invalid choice, please choose from the available choices or press /h for help");
    }

    /**
     * start and launch Client
     * @throws IOException if system error
     */
    private void launch() throws IOException {
        Set<SelectionKey> selectedKeys = selector.selectedKeys();
        Thread listenerThread = new Thread(this::userListenerRun);
        listenerThread.start();
        while (!Thread.interrupted()) {
            selector.select();
            processSelectedKeys();
            userActions();
            selectedKeys.clear();
        }
        listenerThread.interrupt();
    }

    /**
     * Process all selected keys from the selector
     */
    private void processSelectedKeys() {
        for (SelectionKey key : selectedKeys) {
            try {
                if (key.isValid() && key.isAcceptable()) {
                    ((ServerContext) key.attachment()).doAccept(key);
                }
                if (key.isValid() && key.isConnectable()) {
                    ((ClientContext) key.attachment()).doConnect();
                }
                if (key.isValid() && key.isWritable()) {
                    ((Context) key.attachment()).doWrite();
                }
                if (key.isValid() && key.isReadable()) {
                    ((Context) key.attachment()).doRead();
                }
            } catch (IOException e) {
                silentlyClose(key);
            }
        }
    }

    /**
     * silently close the channel from a key
     * @param key a registered key
     */
    private void silentlyClose(SelectionKey key) {
        Channel sc = key.channel();
        try {
            sc.close();
        } catch (IOException e) {
            // ignore exception
        }
    }

    private static void usage() {
        System.out.println("Usage : pseudo ip port");
    }
}
