package fr.upem.java.tcp.matou;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

public class StringReader implements Reader {
    private enum State {DONE,WAITINGINT, WAITINGSTRING, ERROR}

    private final ByteBuffer bb;
    private final Charset UTF8 = Charset.forName("UTF8");
    private State state = State.WAITINGINT;
    private String value;
    private int size;

    StringReader(ByteBuffer bb) {
        this.bb = bb;
    }

    @Override
    public ProcessStatus process() {
        if (state==State.DONE || state==State.ERROR) {
            throw new IllegalStateException();
        }
        bb.flip();
        try {
            switch (state) {
                case DONE:
                    throw new IllegalStateException();

                case WAITINGINT:
                    if (bb.remaining() >= Integer.BYTES) {
                        size = bb.getInt();
                        state = State.WAITINGSTRING;
                    }
                    else{
                        return ProcessStatus.REFILL;
                    }

                case WAITINGSTRING:
                    if (bb.remaining() < size)
                        return ProcessStatus.ERROR;

                    int previousLimit = bb.limit();
                    bb.limit(bb.position() + size);
                    value = UTF8.decode(bb).toString();
                    bb.limit(previousLimit);
                    state = State.DONE;
                    return ProcessStatus.DONE;

                case ERROR:
                    break;
            }
        } finally {
            bb.compact();
        }
        return ProcessStatus.ERROR;
    }

    @Override
    public Object get() {
        if (state!=State.DONE) {
            throw new IllegalStateException();
        }
        return new StringData(value, size);
    }

    @Override
    public void reset() {
        state=State.WAITINGINT;
    }
}
