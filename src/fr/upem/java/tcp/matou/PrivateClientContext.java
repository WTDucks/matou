package fr.upem.java.tcp.matou;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;
import java.util.logging.Logger;

public class PrivateClientContext implements ClientContext, PrivateContext {
    private enum State {
        PENDING,
        LOGIN,
        PRIVATEMSG,
        REQUESTFILESHARING,
        ACCEPTEDFILESHARING,
        REFUSEDFILESHARING,
        RECEIVEFILESHARING,
        ERROR,
    }
    private final Charset UTF8_CHARSET = Charset.forName("UTF-8");
    private final Logger logger = Logger.getLogger(PrivateClientContext.class.getName());
    private final int BUFFER_SIZE = 1024;
    private final String login;
    private final SocketChannel sc;
    private final Client client;
    private ByteBuffer bbin = ByteBuffer.allocateDirect(BUFFER_SIZE);
    private ByteBuffer bbout = ByteBuffer.allocate(BUFFER_SIZE);
    private final Queue<ByteBuffer> queue = new LinkedList<>();
    private final SelectionKey uniqueKey;
    private final IntReader intReader = new IntReader(bbin);
    private final StringReader strReader = new StringReader(bbin);
    private final FileSharingReader fsReader = new FileSharingReader(BUFFER_SIZE, bbin);
    private final FileRequestReader frReader = new FileRequestReader(bbin);
    private final HashMap<String, FileChannelData> fileToShare = new HashMap<>();
    private final HashMap<String, FileChannel> fileToReceive = new HashMap<>();
    private boolean closed = false;
    private boolean connected = false;
    private String destLogin;
    private State clientState = State.PENDING;

    PrivateClientContext(String login, String destLogin, SelectionKey key, Client client) {
        this.login = login;
        this.destLogin = destLogin;
        this.uniqueKey = key;
        this.sc = (SocketChannel) key.channel();
        this.client = client;
        if (destLogin != null) {
            connected = true;
            client.addConnectedUser(destLogin, this);
        }
    }

    /**
     * do connection procedure for a new socketChannel
     * @throws IOException if socketChannel closed
     */
    @Override
    public void doConnect() throws IOException {
        if (!sc.finishConnect())
            return;
        connect();
        updateInterestOps();
    }

    /**
     * Send login request to a Client's private server
     * RFC : 7 | loginSize | login (UTF8)
     * @return always {@code true}
     */
    @Override
    public Boolean connect() {
        bbout.putInt(7);
        bbout.putInt(login.getBytes(UTF8_CHARSET).length);
        bbout.put(UTF8_CHARSET.encode(login));
        updateInterestOps();
        return true;
    }

    /**
     * read from the socketchannel into bbin and process accordingly the RFC
     * (non-blocking)
     * @throws IOException socketChannel closed
     */
    @Override
    public void doRead() throws IOException {
        //int read;
        if (sc.read(bbin) == -1) {
            closed = true;
            logger.info("Connection closed");
        }
        //System.out.println("Receiving : " + read);
        processIn();
        updateInterestOps();
    }

    /**
     * write content of bbout to the socketchannel
     * (non-blocking)
     * @throws IOException socketchannel closed
     */
    @Override
    public void doWrite() throws IOException {
        bbout.flip();
        //System.out.println("Sending : " + sc.write(bbout));
        sc.write(bbout);
        bbout.compact();
        processFileToShare();
        processOut();
        updateInterestOps();
    }

    /**
     * update interestOps of selectedKey according
     */
    @Override
    public void updateInterestOps() {
        int newInterestOps = 0;
        if (!closed && bbin.hasRemaining()) {
            newInterestOps |= SelectionKey.OP_READ;
        }
        if (bbout.position() != 0) {
            newInterestOps |= SelectionKey.OP_WRITE;
        }
        if (newInterestOps == 0) {
            silentlyClose();
        } else {
            uniqueKey.interestOps(newInterestOps);
        }
    }

    /**
     * send a message to a private client context
     * print message on terminal
     * RFC : 8 | msgSize | msg (UTF8)
     * @param msg the message to send
     */
    @Override
    public void sendMessage(String msg) {
        if (!uniqueKey.isValid()) {
            System.out.println("Sorry but " + destLogin + " is no longer here...");
            silentlyClose();
            return;
        }
        int size = msg.getBytes(UTF8_CHARSET).length;
        ByteBuffer toSend = ByteBuffer.allocate(Integer.BYTES * 2 + size);
        toSend.putInt(8);
        toSend.putInt(size);
        toSend.put(UTF8_CHARSET.encode(msg));
        System.out.println("\t(" + login + "): " + msg);
        queuePacket(toSend.flip());
    }

    /**
     * send a request for sharing a file with another user
     * @param path to file
     */
    @Override
    public void sendFileSharingRequest(String path) {
        // code 9
        String filename;
        long size;
        Path p;
        try {
            p = Paths.get(path);
        } catch (InvalidPathException e) {
            System.out.println("Invalid Path");
            return;
        }
        filename = p.getFileName().toString();
        if (!Files.exists(p)){
            System.out.println(filename + " doesn't exist");
            return;
        }
        try {
            size = Files.size(p);
        } catch (IOException e) {
            System.out.println("Error during file read");
            return;
        }
        FileChannel fc;
        try {
            fc = FileChannel.open(p, StandardOpenOption.READ);
        } catch (IOException e) {
            System.out.println("Error opening file: " + filename);
            return;
        }
        int filenameSize = filename.getBytes(UTF8_CHARSET).length;
        ByteBuffer toSend = ByteBuffer.allocate(Integer.BYTES * 2 + Long.BYTES + filenameSize);
        toSend.putInt(9);
        toSend.putInt(filenameSize);
        toSend.put(UTF8_CHARSET.encode(filename));
        toSend.putLong(size);
        System.out.println("Sending request for sharing: " + filename + " to: " + destLogin);
        queuePacket(toSend.flip());
        fileToShare.put(filename, new FileChannelData(fc, false));
    }

    /**
     * send a accept response
     * RFC : 10 | filenameSize | filename (UTF8)
     * @param filename name of file to accept
     */
    @Override
    public void sendAcceptedFileSharing(String filename) {
        //code 10
        if (!fileToReceive.containsKey(filename)) {
            System.out.println(filename + " is not a requested file");
            return;
        }
        FileChannel fc = fileToReceive.get(filename);
        if (fc != null){
            System.out.println(filename + " is already accepted");
            return;
        }
        Path p;
        try {
            p = Paths.get(filename);
        } catch (InvalidPathException e) {
            System.out.println("Invalid Path");
            return;
        }
        if (Files.exists(p)) {
            System.out.println(filename + " already exists");
            return;
        }
        try {
            fc = FileChannel.open(p, StandardOpenOption.CREATE, StandardOpenOption.WRITE);
        } catch (IOException e) {
            System.out.println("Error opening " + filename);
            fileToShare.remove(filename);
            return;
        }
        fileToReceive.put(filename, fc);
        int filenameSize = filename.getBytes(UTF8_CHARSET).length;
        ByteBuffer toSend = ByteBuffer.allocate(Integer.BYTES * 2 + filenameSize);
        toSend.putInt(10);
        toSend.putInt(filenameSize);
        toSend.put(UTF8_CHARSET.encode(filename));
        queuePacket(toSend.flip());
    }

    /**
     * send a refuse response
     * RFC : 11 | filenameSize | filename (UTF8)
     * @param filename name of file to refuse
     */
    @Override
    public void sendRefusedFileSharing(String filename) {
        //code 11
        if (!fileToReceive.containsKey(filename)){
            System.out.println(filename + " is not a requested file");
            return;
        }
        if (fileToReceive.get(filename) != null) {
            System.out.println(filename + " is already accepted");
            return;
        }
        int filenameSize = filename.getBytes(UTF8_CHARSET).length;
        ByteBuffer toSend = ByteBuffer.allocate(Integer.BYTES * 2 + filenameSize);
        toSend.putInt(11);
        toSend.putInt(filenameSize);
        toSend.put(UTF8_CHARSET.encode(filename));
        queuePacket(toSend.flip());
        fileToReceive.remove(filename);


    }

    /**
     * Process bbin content
     */
    private void processIn() {
        while (true) {
            switch (clientState) {
                case PENDING:
                    switch (intReader.process()) {
                        case DONE:
                            int ID = (int) intReader.get();
                            intReader.reset();
                            processID(ID);
                            break;
                        case REFILL:
                            return;
                        case ERROR:
                            logger.warning("Error in processIn -- integer unrecognised");
                            closed = true;
                            return;
                    }
                    break;
                case LOGIN:
                    processLogin();
                    break;
                case PRIVATEMSG:
                    processPrivateMessage();
                    break;
                case REQUESTFILESHARING:
                    processFileSharingRequest();
                    break;
                case ACCEPTEDFILESHARING:
                    processAcceptedFileSharing();
                    break;
                case REFUSEDFILESHARING:
                    processRefusedFileSharing();
                    break;
                case RECEIVEFILESHARING:
                    processFileSharing();
                    break;
                case ERROR:
                    logger.warning("Error in processIn");
                    closed = true;
                    return;
            }
        }
    }

    /**
     * change clientState according to ID received (cf: RFC)
     * @param id id of packet header
     */
    private void processID(int id) {
        switch (id) {
            case 7:
                clientState = State.LOGIN;
                break;
            case 8:
                clientState = State.PRIVATEMSG;
                break;
            case 9:
                clientState = State.REQUESTFILESHARING;
                break;
            case 10:
                clientState = State.ACCEPTEDFILESHARING;
                break;
            case 11:
                clientState = State.REFUSEDFILESHARING;
                break;
            case 12:
                clientState = State.RECEIVEFILESHARING;
                break;
            default:
                logger.warning("processID: ERROR");
                clientState = State.ERROR;
                break;
        }
    }

    /**
     * Process a login request and a client to authorized users
     * if he's in authorized users list
     */
    private void processLogin() {
        switch (strReader.process()) {
            case DONE:
                StringData stringData = (StringData) strReader.get();
                strReader.reset();
                if (client.isUserAuthorized(stringData.getStr())) {
                    destLogin = stringData.getStr();
                    connected = true;
                    client.addConnectedUser(destLogin, this);
                } else {
                    logger.info("illegal user: " + stringData.getStr());
                    closed = true;
                }
                clientState = State.PENDING;
                break;
            case REFILL:
                break;
            case ERROR:
                logger.warning("processLogin: Error");
                clientState = State.ERROR;
                break;
        }
    }

    /**
     * Process a received private message
     */
    private void processPrivateMessage() {
        if (!connected) closed = true;
        switch (strReader.process()) {
            case DONE:
                StringData msg = (StringData) strReader.get();
                strReader.reset();
                System.out.println("\t(" + destLogin + "): " + msg.getStr());
                clientState = State.PENDING;
                break;

            case REFILL:
                break;
            case ERROR:
                logger.warning("Error in processPrivateMessage");
                clientState = State.ERROR;
                break;
        }

    }

    /**
     * Process a request for File Sharing.
     * Send an auto-refuse reply if filename is already downloading or pending
     */
    private void processFileSharingRequest() {
        switch (frReader.process()) {
            case DONE:
                FileRequestData frData = (FileRequestData) frReader.get();
                frReader.reset();
                System.out.println(
                        "File Sharing: " +
                        frData.getFilename().getStr() +
                        "(" + frData.getFilesize() + "Bytes) Requested by: " +
                        destLogin
                );
                if (!fileToReceive.containsKey(frData.getFilename().getStr()))
                    fileToReceive.put(frData.getFilename().getStr(), null);
                else{
                    System.out.println(
                            "Auto Refuse: a file with the same filename is already downloading or pending: " +
                            frData.getFilename().getStr()
                    );
                    sendRefusedFileSharing(frData.getFilename().getStr());
                }
                clientState = State.PENDING;
                break;
            case REFILL:
                break;
            case ERROR:
                logger.warning("Error in processFileSharingRequest");
                clientState = State.ERROR;
                break;
        }
    }

    /**
     * get a file (if usable), create a packet from it (max : 1024bytes)
     * and add to the packetQueue for sending
     * RFC : 12 | filenameSize | filename | data (bytes)
     * @param filename file to process
     */
    private void pushFileToQueue(String filename) {
        int read;
        if (!fileToShare.containsKey(filename))
            return;

        FileChannelData fcd = fileToShare.get(filename);
        if (!fcd.isUsable())
            return;
        FileChannel fc = fcd.getFc();
        ByteBuffer toSend = ByteBuffer.allocate(BUFFER_SIZE);
        toSend.putInt(12);
        toSend.putInt(filename.getBytes(UTF8_CHARSET).length);
        toSend.put(UTF8_CHARSET.encode(filename));
        try {
            read = fc.read(toSend);
        } catch (IOException e) {
            System.out.println("Error reading " + filename);
            try {
                fileToShare.remove(filename).getFc().close();
            } catch (IOException e1) {
                System.out.println("Error closing " + filename);
            }
            return;
        }
        if (read == -1) {
            System.out.println("Sending " + filename + " completed");
            try {
                fileToShare.remove(filename).getFc().close();
            } catch (IOException e) {
                System.out.println("Error closing " + filename);
            }
            return;
        }
        queuePacket(toSend.flip());
    }

    /**
     * Process an Accepted File Sharing response.
     * Toggle FileChannelData.isUsable to true
     */
    private void processAcceptedFileSharing() {
        switch (strReader.process()){
            case DONE:
                StringData strData = (StringData) strReader.get();
                strReader.reset();
                if (fileToShare.containsKey(strData.getStr()))
                    fileToShare.get(strData.getStr()).setUsable(true);
                clientState = State.PENDING;
                break;
            case REFILL:
                break;
            case ERROR:
                logger.warning("Error in processAcceptedFileSharing");
                clientState = State.ERROR;
                break;
        }
    }

    /**
     * Process a Refused File Sharing response.
     * Remove filename from FileToShare Database
     */
    private void processRefusedFileSharing() {
        switch (strReader.process()) {
            case DONE:
                StringData strData = (StringData) strReader.get();
                strReader.reset();
                if (fileToShare.containsKey(strData.getStr())) {
                    System.out.println("File Sharing: " + strData.getStr() + "Refused by: " + destLogin);
                    try {
                        fileToShare.remove(strData.getStr()).getFc().close();
                    } catch (IOException e) {
                        System.out.println("Error during " + strData.getStr() + " closing");
                    }
                }
                clientState = State.PENDING;
                break;
            case REFILL:
                break;
            case ERROR:
                logger.warning("Error in processRefusedFileSharing");
                clientState = State.ERROR;
                break;
        }
    }

    /**
     * Check every file from the fileToShare Database
     * and push it for sending if is usable
     */
    private void processFileToShare() {
        fileToShare.forEach((filename, fcd) -> {
            if (fcd.isUsable())
                pushFileToQueue(filename);
        });
    }

    /**
     * write the received data into the suitable FileChannel
     */
    private void processFileSharing() {
        //code 12
        switch (fsReader.process()){
            case DONE:
                FileSharingData fsData = (FileSharingData) fsReader.get();
                fsReader.reset();
                if (fileToReceive.containsKey(fsData.getFilename().getStr())) {
                    FileChannel fs = fileToReceive.get(fsData.getFilename().getStr());
                    try {
                        fs.write(fsData.getData());
                    } catch (IOException e) {
                        System.out.println("Error during writting on :" + fsData.getFilename().getStr());
                        try {
                            fileToReceive.remove(fsData.getFilename().getStr()).close();
                        } catch (IOException e1) {
                            System.out.println("Error during closing: " + fsData.getFilename().getStr());
                        }
                    }
                }
                clientState = State.PENDING;
                break;
            case REFILL:
                break;
            case ERROR:
                logger.warning("Error in processFileSharing");
                clientState = State.ERROR;
                break;
        }


    }

    /**
     * Add a bytebuffer (in READ mode) to the packet queue, tries to fill bbOut and updateInterestOps
     *
     * @param packet packet to add (has to be in READ mode)
     */
    private void queuePacket(ByteBuffer packet) {
        queue.add(packet);
        processOut();
        updateInterestOps();
    }

    /**
     * Try to fill bbout from the message queue
     * Every packet used in this method has to be in READ mode
     * At the end, every packet used are stil in READ mode
     */
    private void processOut() {
        while (!queue.isEmpty()) {
            ByteBuffer bb = queue.peek();
            if (bb == null) continue;
            if (bbout.remaining() > bb.limit()) {
                queue.poll();
                bbout.put(bb);
                bb.flip();
            }
        }
    }

    /**
     * silently close the client
     */
    private void silentlyClose() {
        try {
            sc.close();
            client.removeConnectedUser(destLogin);
            client.removeAuthorizedUser(destLogin);
        } catch (IOException e) {
            // ignore exception
        }
    }
}
