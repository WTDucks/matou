package fr.upem.java.tcp.matou;

import java.nio.ByteBuffer;

public class MessageClientReader implements Reader {

    private enum State {DONE, WAITINGLOGIN, WAITINGMSG, ERROR}

    private final ByteBuffer bb;
    private State state = State.WAITINGLOGIN;
    private String login;
    private String msg;

    MessageClientReader(ByteBuffer bb) {
        this.bb = bb;
    }

    /**
     * Process the buffer to get information.
     * login and message
     * @return state after process
     */
    @Override
    public ProcessStatus process() {
        StringReader sr = new StringReader(bb);
        switch (state) {
            case DONE:
                throw new IllegalStateException();

            case ERROR:
                throw new IllegalStateException();

            case WAITINGLOGIN:
                switch (sr.process()) {
                    case DONE:
                        StringData sd = (StringData) sr.get();
                        login = sd.getStr();
                        state = State.WAITINGMSG;
                        sr.reset();
                        break;

                    case REFILL:
                        return ProcessStatus.REFILL;

                    case ERROR:
                        return ProcessStatus.ERROR;
                }

            case WAITINGMSG:
                switch (sr.process()) {
                    case DONE:
                        StringData sd = (StringData) sr.get();
                        msg = sd.getStr();
                        state = State.DONE;
                        sr.reset();
                        return ProcessStatus.DONE;

                    case REFILL:
                        return ProcessStatus.REFILL;

                    case ERROR:
                        return ProcessStatus.ERROR;
                }
        }
        return ProcessStatus.ERROR;
    }

    /**
     * get reader information
     * @return {@code MessageData} value
     */
    @Override
    public Object get() {
        if (state != State.DONE) {
            throw new IllegalStateException();
        }
        return new MessageData(login, msg);
    }

    /**
     * reset reader
     */
    @Override
    public void reset() {
        state = State.WAITINGLOGIN;
    }
}
