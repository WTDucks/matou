package fr.upem.java.tcp.matou;

import java.io.IOException;
import java.nio.channels.SelectionKey;

public interface ServerContext {
    void doAccept(SelectionKey key) throws IOException;
}
