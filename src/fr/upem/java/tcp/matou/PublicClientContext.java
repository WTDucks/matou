package fr.upem.java.tcp.matou;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.logging.Logger;

public class PublicClientContext implements ClientContext {

    private enum State {
        PENDING,
        RECEIVEGLOBALMSG,
        RECEIVECONNECTIONREQUEST,
        ERROR,
        ACCEPTEDCONNECTIONREQUEST,
        REFUSEDCONNECTIONREQUEST,
    }
    private final Charset UTF8_CHARSET = Charset.forName("UTF-8");
    private final Logger logger = Logger.getLogger(Client.class.getName());
    private final int BUFFER_SIZE = 1024;
    private final String login;
    private final SocketChannel sc;
    private final Client client;
    private final SelectionKey uniqueKey;
    private final ByteBuffer bbin = ByteBuffer.allocateDirect(BUFFER_SIZE);
    private final ByteBuffer bbout = ByteBuffer.allocate(BUFFER_SIZE);
    private final IntReader intReader = new IntReader(bbin);
    private final MessageClientReader mcReader = new MessageClientReader(bbin);
    private final StringReader strReader = new StringReader(bbin);
    private final AcceptedPrivateMessageReader apmReader = new AcceptedPrivateMessageReader(bbin);
    private State clientState = State.PENDING;
    private boolean closed = false;

    PublicClientContext(Client client, SelectionKey key, String login) {
        this.uniqueKey = key;
        this.sc = (SocketChannel) key.channel();
        this.client = client;
        this.login = login;
    }

    @Override
    public Boolean connect() {
        int loginSize = login.getBytes(UTF8_CHARSET).length;
        if (loginSize == 0)
            return false;
        if (Integer.BYTES + loginSize > bbout.remaining()) {
            logger.warning("login too long, please use a smaller login name than: " + login);
            return false;
        } else {
            bbout.putInt(0);
            bbout.putInt(loginSize);
            bbout.put(UTF8_CHARSET.encode(login));
            System.out.println("Connecting to server with " + login);
            updateInterestOps();
            return true;
        }
    }

    @Override
    public void doConnect() throws IOException {
        if (!sc.finishConnect())
            return;
        if (!connect())
            closed = true;
        updateInterestOps();
    }

    @Override
    public void doRead() throws IOException {
        if (sc.read(bbin) == -1) {
            closed = true;
            logger.info("Connection closed");
        }
        processIn();
        updateInterestOps();
    }

    @Override
    public void doWrite() throws IOException {
        bbout.flip();
        //System.out.println("Sending : " + sc.write(bbout));
        sc.write(bbout);
        bbout.compact();
        updateInterestOps();
    }

    @Override
    public void updateInterestOps() {
        int newInterestOps = 0;

        if (!closed && bbin.hasRemaining()) {
            newInterestOps |= SelectionKey.OP_READ;
        }
        if (bbout.position() != 0) {
            newInterestOps |= SelectionKey.OP_WRITE;
        }
        if (newInterestOps == 0) {
            silentlyClose();
        } else {
            uniqueKey.interestOps(newInterestOps);
        }
    }

    private void silentlyClose() {
        try {
            sc.close();
        } catch (IOException e) {
            // ignore exception
        }
    }

    /**
     * Send to the server a global message
     * @param msg the message to send
     */
    void sendGlobalMessage(String msg) {
        // 3 - Global Message
        if (!uniqueKey.isValid()) {
            System.out.println("Sorry but the server is no longer available");
            silentlyClose();
            return;
        }
        ByteBuffer bb2Send = UTF8_CHARSET.encode(msg);
        int sizeBB = msg.getBytes(UTF8_CHARSET).length;
        if (bbout.remaining() < Integer.BYTES * 2 + sizeBB) {
            System.out.println("Message is too long");
            return;
        }
        bbout.putInt(3);
        bbout.putInt(sizeBB);
        bbout.put(bb2Send);
        updateInterestOps();
    }

    void sendPersonalMessageRequest(String requestedUser) {
        if (!uniqueKey.isValid()) {
            System.out.println("Sorry but the server is no longer available");
            silentlyClose();
            return;
        }
        int size = requestedUser.getBytes(UTF8_CHARSET).length;
        bbout.putInt(4);
        bbout.putInt(size);
        bbout.put(UTF8_CHARSET.encode(requestedUser));
        System.out.println("Private Conversation Requested to: " + requestedUser);
        updateInterestOps();
    }

    void sendAcceptedMessageRequest(String sender) {
        if (!uniqueKey.isValid()) {
            System.out.println("Sorry but the server is no longer available");
            silentlyClose();
            return;
        }
        InetSocketAddress serverInfo;
        try {
            serverInfo = client.getServerInfo();
        } catch (IOException e) {
            logger.warning("serverInfo error");
            sendRefusedMessageRequest(sender);
            return;
        }
        int senderSize = sender.getBytes(UTF8_CHARSET).length;
        StringData IPAddress = new StringData(
                serverInfo.getAddress().getHostAddress(),
                serverInfo.getAddress().getHostAddress().getBytes(UTF8_CHARSET).length
        );
        bbout.putInt(5);
        bbout.putInt(senderSize);
        bbout.put(UTF8_CHARSET.encode(sender));
        bbout.putInt(IPAddress.getSize());
        bbout.put(UTF8_CHARSET.encode(IPAddress.getStr()));
        bbout.putInt(serverInfo.getPort());
        client.addAuthorizedUser(sender);
        updateInterestOps();
    }

    void sendRefusedMessageRequest(String sender) {
        if (!uniqueKey.isValid()) {
            System.out.println("Sorry but the server is no longer available");
            silentlyClose();
            return;
        }
        int size = sender.getBytes(UTF8_CHARSET).length;
        bbout.putInt(6);
        bbout.putInt(size);
        bbout.put(UTF8_CHARSET.encode(sender));
        updateInterestOps();
    }

    private void processIn() {
        while (true) {
            switch (clientState) {
                case PENDING:
                    switch (intReader.process()) {
                        case DONE:
                            int ID = (int) intReader.get();
                            intReader.reset();
                            processID(ID);
                            break;
                        case REFILL:
                            return;
                        case ERROR:
                            logger.warning("Error in processIn -- integer unrecognised");
                            closed = true;
                            return;
                    }
                    break;

                case RECEIVEGLOBALMSG:
                    processGlobalMessage();
                    break;

                case RECEIVECONNECTIONREQUEST:
                    processConnectionRequest();
                    break;

                case ACCEPTEDCONNECTIONREQUEST:
                    processAcceptedConnection();
                    break;

                case REFUSEDCONNECTIONREQUEST:
                    processRefusedConnection();
                    break;

                case ERROR:
                    logger.warning("Error in processIn");
                    closed = true;
                    return;
            }
        }
    }

    private void processGlobalMessage() {
        switch (mcReader.process()) {
            case DONE:
                MessageData md = (MessageData) mcReader.get();
                mcReader.reset();
                System.out.println("\t" + md.getLogin() + ": " + md.getMsg());
                clientState = State.PENDING;
                break;

            case REFILL:
                break;

            case ERROR:
                logger.warning("ERROR in processGlobalMessage");
                clientState = State.ERROR;
                break;
        }
    }

    private void processID(int ID) {
        switch (ID) {
            case 1:
                System.out.println("Connection succeed... Welcome " + login);
                clientState = State.PENDING;
                break;

            case 2:
                clientState = State.PENDING;
                System.out.println("Username already used... Please choose another.");
                closed = true;
                break;

            case 3:
                clientState = State.RECEIVEGLOBALMSG;
                break;

            case 4:
                clientState = State.RECEIVECONNECTIONREQUEST;
                break;

            case 5:
                clientState = State.ACCEPTEDCONNECTIONREQUEST;
                break;
            case 6:
                clientState = State.REFUSEDCONNECTIONREQUEST;
                break;

            default:
                logger.info("processID: unrecognised packet ID");
                clientState = State.ERROR;
                break;
        }
    }

    private void processConnectionRequest() {
        switch (strReader.process()) {
            case DONE:
                StringData md = (StringData) strReader.get();
                strReader.reset();
                System.out.println("Private Conversation Requested by: " + md.getStr());
                clientState = State.PENDING;
                break;
            case REFILL:
                break;
            case ERROR:
                logger.warning("processConnectionRequest: Error in process");
                clientState = State.ERROR;
                break;
        }
    }

    private void processAcceptedConnection() {
        PrivateMessageConnectionData data;
        switch (apmReader.process()){
            case DONE:
                data = (PrivateMessageConnectionData) apmReader.get();
                apmReader.reset();
                break;
            case REFILL:
                return;
            case ERROR:
                logger.warning("processAcceptedConnection: error in process");
                clientState = State.ERROR;
                return;
            default:
                logger.warning("processAcceptedConnection: error in process (default)");
                clientState = State.ERROR;
                return;

        }
        SocketChannel newClient;
        try {
            newClient = SocketChannel.open();
            newClient.bind(null);
            newClient.configureBlocking(false);
            newClient.connect(data.getClientAddress());
        } catch (IOException e) {
            logger.severe("processAcceptedConnection: Internal error");
            return;
        }
        client.registerPrivateClient(newClient, data.getLogin().getStr(), SelectionKey.OP_CONNECT);
        System.out.println("Private Conversation Accepted by: " + data.getLogin().getStr());
        clientState = State.PENDING;
    }

    private void processRefusedConnection() {
        switch (strReader.process()) {
            case DONE:
                StringData md = (StringData) strReader.get();
                strReader.reset();
                System.out.println("Private Conversation Refused by: " + md.getStr());
                clientState = State.PENDING;
                break;
            case REFILL:
                break;
            case ERROR:
                logger.warning("processRefusedConnection: Error in process");
                clientState = State.ERROR;
                break;
        }
    }
}
