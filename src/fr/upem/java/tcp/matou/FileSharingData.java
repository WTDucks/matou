package fr.upem.java.tcp.matou;

import java.nio.ByteBuffer;

class FileSharingData {
    private final StringData filename;
    private final ByteBuffer data;

    FileSharingData(StringData filename, ByteBuffer data) {
        this.filename = filename;
        this.data = data;
    }

    StringData getFilename() {
        return filename;
    }

    ByteBuffer getData() {
        return data;
    }
}
