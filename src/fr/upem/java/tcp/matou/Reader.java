package fr.upem.java.tcp.matou;


public interface Reader {

    enum ProcessStatus {DONE,REFILL,ERROR}

    ProcessStatus process();

    Object get();

    void reset();

}
