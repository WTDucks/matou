package fr.upem.java.tcp.matou;

import java.io.IOException;

public interface ClientContext extends Context {
    void doConnect() throws IOException;
    Boolean connect();
}
