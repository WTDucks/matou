package fr.upem.java.tcp.matou;

import java.util.Objects;

class StringData {
    private String str;
    private int size;

    StringData(String str, int size) {
        Objects.requireNonNull(str);
        this.str = str;
        this.size = size;
    }

    String getStr() {
        return str;
    }

    int getSize() {
        return size;
    }
}
