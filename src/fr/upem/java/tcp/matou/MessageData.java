package fr.upem.java.tcp.matou;

import java.util.Objects;

class MessageData {
    private String login;
    private String msg;

    MessageData(String login, String msg) {
        Objects.requireNonNull(login);
        Objects.requireNonNull(msg);
        this.login = login;
        this.msg = msg;
    }

    String getLogin() {
        return login;
    }

    String getMsg() {
        return msg;
    }
}