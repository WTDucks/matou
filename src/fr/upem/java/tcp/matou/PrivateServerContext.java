package fr.upem.java.tcp.matou;

import java.io.IOException;
import java.nio.channels.SelectionKey;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

public class PrivateServerContext implements ServerContext {
    private final Client client;

    PrivateServerContext(Client client) {
        this.client = client;
    }

    @Override
    public void doAccept(SelectionKey key) throws IOException {
        ServerSocketChannel ssc = (ServerSocketChannel) key.channel();
        SocketChannel client = ssc.accept();
        if (client == null)
            return;
        client.configureBlocking(false);
        this.client.registerPrivateClient(client, null, SelectionKey.OP_READ);
    }
}
