package fr.upem.java.tcp.matou;

import java.nio.channels.FileChannel;

class FileChannelData {
    private final FileChannel fc;
    private Boolean usable;

    FileChannelData(FileChannel fc, Boolean usable) {
        this.fc = fc;
        this.usable = usable;
    }

    FileChannel getFc() {
        return fc;
    }

    Boolean isUsable() {
        return usable;
    }

    void setUsable(Boolean b){
        this.usable = b;
    }
}
