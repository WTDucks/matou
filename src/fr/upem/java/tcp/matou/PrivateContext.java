package fr.upem.java.tcp.matou;

public interface PrivateContext {
    void sendMessage(String msg);
    void sendFileSharingRequest(String path);
    void sendAcceptedFileSharing(String filename);
    void sendRefusedFileSharing(String filename);
}
