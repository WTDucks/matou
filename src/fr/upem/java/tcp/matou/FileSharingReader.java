package fr.upem.java.tcp.matou;

import java.nio.ByteBuffer;

public class FileSharingReader implements Reader {
    private enum State {DONE, WAITINGFILENAME, WAITINGDATA, ERROR}
    private final int BUFFER_SIZE;
    private final ByteBuffer bb;
    private StringData filename;
    private ByteBuffer data;
    private State state = State.WAITINGFILENAME;
    private final StringReader stringReader;

    FileSharingReader(int buffer_size, ByteBuffer bb) {
        this.BUFFER_SIZE = buffer_size;
        this.bb = bb;
        this.stringReader = new StringReader(bb);
    }

    /**
     * Process the buffer to get information.
     * filename and a bytebuffer containing partial data of file
     * @return state after process
     */
    @Override
    public ProcessStatus process() {
        if (state==State.DONE || state==State.ERROR) {
            throw new IllegalStateException();
        }
        switch (state) {
            case WAITINGFILENAME:
                switch (stringReader.process()){
                    case DONE:
                        filename = (StringData) stringReader.get();
                        stringReader.reset();
                        state = State.WAITINGDATA;
                        data = ByteBuffer.allocate(BUFFER_SIZE - Integer.BYTES * 2 - filename.getSize());
                        break;
                    case REFILL:
                        return ProcessStatus.REFILL;
                    case ERROR:
                        return ProcessStatus.ERROR;
                }

            case WAITINGDATA:
                if (data.hasRemaining()){
                    data.put(bb);
                    if(!data.hasRemaining())
                        return ProcessStatus.DONE;
                    else
                        return ProcessStatus.REFILL;
                }
                return ProcessStatus.DONE;
        }
        return ProcessStatus.ERROR;
    }

    @Override
    public Object get() {
        if (state!=State.DONE) {
            throw new IllegalStateException();
        }
        return new FileSharingData(filename, data.flip());
    }

    @Override
    public void reset() {
        state = State.WAITINGFILENAME;
    }
}
