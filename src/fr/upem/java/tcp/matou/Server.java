package fr.upem.java.tcp.matou;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.nio.charset.Charset;
import java.util.*;
import java.util.logging.Logger;

public class Server {

    static private class Context {

        private enum State {
            PENDING,
            LOGIN,
            GLOBALMSG,
            ERROR,
            REQUESTPM,
            ACCEPTMSG,
            REFUSEMSG,
        }

        final private SelectionKey key;
        final private SocketChannel sc;
        final private ByteBuffer bbin = ByteBuffer.allocate(BUFFER_SIZE);
        final private ByteBuffer bbout = ByteBuffer.allocate(BUFFER_SIZE);
        final private Queue<ByteBuffer> queue = new LinkedList<>();
        final private Server server;
        private boolean closed = false;
        private boolean connected = false;
        private StringData login = null;
        private State clientState = State.PENDING;
        private static final Charset UTF8_CHARSET = Charset.forName("UTF-8");
        private static final Logger logger = Logger.getLogger(Client.class.getName());
        private static final int BUFFER_SIZE = 1024;


        final private IntReader intReader = new IntReader(bbin);
        final private StringReader stringReader = new StringReader(bbin);
        final private AcceptedPrivateMessageReader apmReader = new AcceptedPrivateMessageReader(bbin);

        private Context(Server server, SelectionKey key) {
            this.key = key;
            this.sc = (SocketChannel) key.channel();
            this.server = server;
        }

        /**
         * Process the content of bbin
         * <p>
         * The convention is that bbin is in write-mode before the call
         * to process and after the call
         */
        private void processIn() {
            while (true) {
                switch (clientState) {
                    case PENDING:
                        // le client vient de se reveiller
                        // on commence par lire l'ID de son paquet
                        switch (intReader.process()) {
                            case DONE:
                                // on à l'ID maintenant...
                                int ID = (int) intReader.get();
                                intReader.reset();
                                processID(ID);
                                break;

                            case REFILL:
                                return;

                            case ERROR:
                                logger.warning("Error in processIn -- intReader unrecognised");
                                silentlyClose();
                                return;
                        }
                        break;

                    case LOGIN:
                        // le client demande à se loger
                        processLogin();
                        break;

                    case GLOBALMSG:
                        // le client demande à envoyer un message globale
                        processGlobalMessage();
                        break;

                    case REQUESTPM:
                        processPrivateMessageRequest();
                        break;

                    case ACCEPTMSG:
                        processAcceptedPrivateMessageRequest();
                        break;

                    case REFUSEMSG:
                        processRefusedPrivateMessageRequest();
                        break;

                    case ERROR:
                        logger.warning("Invalid ID Packet...");
                        silentlyClose();
                        return;
                }
            }
        }

        /**
         * Update the client State based on the given ID
         *
         * @param ID (see RFC)
         */
        private void processID(int ID) {
            switch (ID) {
                case 0:
                    clientState = State.LOGIN;
                    break;
                case 3:
                    clientState = State.GLOBALMSG;
                    break;
                case 4:
                    clientState = State.REQUESTPM;
                    break;
                case 5:
                    clientState = State.ACCEPTMSG;
                    break;
                case 6:
                    clientState = State.REFUSEMSG;
                    break;
                default:
                    logger.warning("Error in processID");
                    silentlyClose();
                    break;
            }
        }

        /**
         * Process the Login request of client
         * RFC : 2 (if refused)
         * RFC : 3 (if accepted)
         * When DONE, client state is set to PENDING
         */
        private void processLogin() {
            switch (stringReader.process()) {
                case DONE:
                    // on à le login maintenant...
                    StringData stringData = (StringData) stringReader.get();
                    stringReader.reset();

                    if (!server.putClient(stringData.getStr(), this)) {
                        //client refusé, envoi 2
                        queuePacket(ByteBuffer.allocate(Integer.BYTES).putInt(2).flip());
                    } else {
                        //client accepté, envoi 1
                        connected = true;
                        login = stringData;
                        queuePacket(ByteBuffer.allocate(Integer.BYTES).putInt(1).flip());
                    }
                    clientState = State.PENDING;
                    break;

                case REFILL:
                    break;

                case ERROR:
                    logger.warning("Error on the processLogin");
                    silentlyClose();
                    break;
            }
        }

        /**
         * Process the Global Message request of client
         * RFC : 3 | loginSize | loginSender(UTF8) | msgSize | msg (UTF8)
         * When DONE, client state is set to PENDING
         */
        private void processGlobalMessage() {
            if (!connected) silentlyClose();
            switch (stringReader.process()) {
                case DONE:
                    StringData msg = (StringData) stringReader.get();
                    stringReader.reset();
                    ByteBuffer toSend = ByteBuffer.allocate(Integer.BYTES * 3 + login.getSize() + msg.getSize());
                    toSend.putInt(3);
                    toSend.putInt(login.getSize());
                    toSend.put(UTF8_CHARSET.encode(login.getStr()));
                    toSend.putInt(msg.getSize());
                    toSend.put(UTF8_CHARSET.encode(msg.getStr()));
                    server.broadcast(toSend.flip());
                    clientState = State.PENDING;
                    break;

                case REFILL:
                    break;
                case ERROR:
                    logger.warning("Error in processGlobalMessage -- clientstate unrecognised");
                    silentlyClose();
                    break;
            }
        }


        /**
         * send to the sender that his request for private message has been rejected
         * RFC : 6 | loginSize | loginSender(UTF8)
         * @param sender Context of the sender
         * @param requestedLogin login of the requested user
         */
        private void sendRefusedPrivateMessageRequest(Context sender, String requestedLogin) {
            int loginSize = requestedLogin.getBytes(UTF8_CHARSET).length;
            ByteBuffer toSend = ByteBuffer.allocate(2 * Integer.BYTES + loginSize);
            toSend.putInt(6);
            toSend.putInt(loginSize);
            toSend.put(UTF8_CHARSET.encode(requestedLogin));
            sender.queuePacket(toSend.flip());
        }

        /**
         * Send to requested user a request for Private Message from sender
         * RFC : 4 | loginSize | loginSender(UTF8)
         * When DONE, client state is set to PENDING
         */
        private void processPrivateMessageRequest() {
            if (!connected) silentlyClose();
            switch (stringReader.process()) {
                case DONE:
                    StringData requestedUserLogin = (StringData) stringReader.get();
                    stringReader.reset();
                    Optional<Context> requestedClient = server.getClient(requestedUserLogin.getStr());
                    if (requestedClient.isPresent()) {
                        ByteBuffer bb = ByteBuffer.allocate(
                                Integer.BYTES * 3
                                        + login.getSize()
                                        + requestedUserLogin.getSize()
                        );
                        bb.putInt(4);
                        bb.putInt(login.getSize());
                        bb.put(UTF8_CHARSET.encode(login.getStr()));
                        server.sendToClient(bb.flip(), requestedClient.get());
                    } else {
                        sendRefusedPrivateMessageRequest(this, requestedUserLogin.getStr());
                    }
                    clientState = State.PENDING;
                    break;

                case REFILL:
                    break;

                case ERROR:
                    logger.warning("Error in processPrivateMessageRequest");
                    silentlyClose();
                    break;
            }
        }

        /**
         * Process Accepted Request for Private Message and send to sender information about
         * requested user's server
         * RFC : 5 | loginSize | loginRequestedUser(UTF8) | IP size | IP (UTF8) | Port
         * When DONE, client state is set to PENDING
         */
        private void processAcceptedPrivateMessageRequest() {
            StringData sender = null;
            InetSocketAddress requestedClientAddress = null;
            switch (apmReader.process()) {
                case DONE:
                    sender = ((PrivateMessageConnectionData) apmReader.get()).getLogin();
                    requestedClientAddress = ((PrivateMessageConnectionData) apmReader.get()).getClientAddress();
                    apmReader.reset();
                    break;
                case REFILL:
                    return;
                case ERROR:
                    logger.warning("ERROR : processAcceptedPrivateMessageRequest");
                    silentlyClose();
                    return;
            }
            StringData requestedClientIPAddress = new StringData(
                    requestedClientAddress.getAddress().getHostAddress(),
                    requestedClientAddress.getAddress().getHostAddress().getBytes(UTF8_CHARSET).length
            );
            ByteBuffer toSend = ByteBuffer.allocate(
                    4 * Integer.BYTES
                            + login.getSize()
                            + requestedClientIPAddress.getSize()
            );
            toSend.putInt(5);
            toSend.putInt(login.getSize());
            toSend.put(UTF8_CHARSET.encode(login.getStr()));
            toSend.putInt(requestedClientIPAddress.getSize());
            toSend.put(UTF8_CHARSET.encode(requestedClientIPAddress.getStr()));
            toSend.putInt(requestedClientAddress.getPort());

            Optional<Context> senderContext = server.getClient(sender.getStr());
            if (senderContext.isPresent()) {
                server.sendToClient(toSend.flip(), senderContext.get());
            } else {
                logger.warning("processAcceptedPrivateMessageRequest : invalid user");
            }
            clientState = State.PENDING;
        }

        /**
         * Process Refused Request for Private Message and send to sender the response
         * RFC : 6 | loginSize | loginRequestedUser(UTF8)
         * When DONE, client state is set to PENDING
         */
        private void processRefusedPrivateMessageRequest() {
            StringData sender;
            switch (stringReader.process()) {
                case DONE:
                    sender = (StringData) stringReader.get();
                    stringReader.reset();
                    ByteBuffer toSend = ByteBuffer.allocate(2 * Integer.BYTES + login.getSize());
                    toSend.putInt(6);
                    toSend.putInt(login.getSize());
                    toSend.put(UTF8_CHARSET.encode(login.getStr()));
                    Optional<Context> senderContext = server.getClient(sender.getStr());
                    if (senderContext.isPresent()) {
                        server.sendToClient(toSend.flip(), senderContext.get());
                    } else {
                        logger.warning("processRefusedPrivateMessageRequest : invalid user");
                    }
                    clientState = State.PENDING;
                    break;
                case REFILL:
                    break;
                case ERROR:
                    logger.warning("Invalid login : processRefusedPrivateMessageRequest");
                    silentlyClose();
            }
        }

        /**
         * Add a bytebuffer (in READ mode) to the packet queue, tries to fill bbOut and updateInterestOps
         *
         * @param packet packet to add (has to be in READ mode)
         */
        private void queuePacket(ByteBuffer packet) {
            queue.add(packet);
            processOut();
            updateInterestOps();
        }

        /**
         * Try to fill bbout from the message queue
         * Every packet used in this method has to be in READ mode
         * At the end, every packet used are stil in READ mode
         */
        private void processOut() {
            while (!queue.isEmpty()) {
                ByteBuffer bb = queue.peek();
                if (bb == null) continue;
                if (bbout.remaining() > bb.limit()) {
                    queue.poll();
                    bbout.put(bb);
                    bb.flip();
                }
            }
        }

        /**
         * Update the interestOps of the key looking
         * only at values of the boolean closed and
         * of both ByteBuffers.
         * <p>
         * The convention is that both buffers are in write-mode before the call
         * to updateInterestOps and after the call.
         * Also it is assumed that process has been be called just
         * before updateInterestOps.
         */
        private void updateInterestOps() {
            int newInterestOps = 0;

            if (!closed && bbin.hasRemaining()) {
                newInterestOps |= SelectionKey.OP_READ;
            }
            if (bbout.position() != 0 || !queue.isEmpty()) {
                newInterestOps |= SelectionKey.OP_WRITE;
            }
            if (newInterestOps == 0) {
                silentlyClose();
            } else {
                key.interestOps(newInterestOps);
            }
        }

        /**
         * silently close the client
         */
        private void silentlyClose() {
            try {
                sc.close();
            } catch (IOException e) {
                // ignore exception
            }
        }

        /**
         * Performs the read action on sc
         * <p>
         * The convention is that both buffers are in write-mode before the call
         * to doRead and after the call
         *
         * @throws IOException if socketChannel is closed
         */
        private void doRead() throws IOException {
            if (sc.read(bbin) == -1) {
                closed = true;
            }
            processIn();
            updateInterestOps();
        }

        /**
         * Performs the write action on sc
         * <p>
         * The convention is that both buffers are in write-mode before the call
         * to doWrite and after the call
         *
         * @throws IOException if socketChannel is closed
         */
        private void doWrite() throws IOException {
            bbout.flip();
            sc.write(bbout);
            bbout.compact();
            processOut();
            updateInterestOps();
        }

    }

    /**
     * Add a packet to all connected clients queue
     *
     * @param packet The message to send to all (in Read mode)
     */
    private void broadcast(ByteBuffer packet) {
        for (SelectionKey key : selector.keys()) {
            Context context = (Context) key.attachment();
            if (context != null) {
                context.queuePacket(packet);
            }
        }
    }

    /**
     * Add to a specific client (Context) a bytebuffer to his queue
     *
     * @param packet a packet that respect the RFC
     * @param client the context of the client to add
     */
    private void sendToClient(ByteBuffer packet, Context client) {
        client.queuePacket(packet);
    }

    /**
     * get the Context of connected user
     * @param login the login of the client
     * @return an {@code Optional} with a present value if the specified value
     * is non-{@code null}, otherwise an empty {@code Optional}
     */
    private Optional<Context> getClient(String login) {
        return Optional.ofNullable(clientDB.get(login));
    }

    /**
     * Put a client and his {@code Context} to the Database
     * @param login username
     * @param client Context
     * @return {@code true} if login has been register in Database
     * otherwise {@code false}
     */
    private boolean putClient(String login, Context client) {
        return clientDB.putIfAbsent(login, client) == null;
    }

    /**
     * Delete a client from the Database
     * @param login username to remove
     */
    private void deleteClient(String login) {
        clientDB.remove(login);
        System.out.println(login + " removed from db");
    }

    private final ServerSocketChannel serverSocketChannel;
    private final Selector selector;
    private final Set<SelectionKey> selectedKeys;

    private final HashMap<String, Context> clientDB = new HashMap<>();


    private Server(int port) throws IOException {
        serverSocketChannel = ServerSocketChannel.open();
        serverSocketChannel.bind(new InetSocketAddress(port));
        selector = Selector.open();
        selectedKeys = selector.selectedKeys();
    }

    /**
     * launch server
     * @throws IOException if system error
     */
    private void launch() throws IOException {
        serverSocketChannel.configureBlocking(false);
        serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
        Set<SelectionKey> selectedKeys = selector.selectedKeys();
        while (!Thread.interrupted()) {
            printKeys();
            System.out.println("Starting select");
            selector.select();
            System.out.println("Select finished");
            printSelectedKey();
            processSelectedKeys();
            selectedKeys.clear();
        }
    }

    /**
     * Process all selected keys from selector
     * @throws IOException if system can no longer accept
     */
    private void processSelectedKeys() throws IOException {
        for (SelectionKey key : selectedKeys) {
            if (key.isValid() && key.isAcceptable()) {
                doAccept(key);
            }
            try {
                if (key.isValid() && key.isWritable()) {
                    ((Context) key.attachment()).doWrite();
                }
                if (key.isValid() && key.isReadable()) {
                    ((Context) key.attachment()).doRead();
                }
            } catch (IOException e) {
                silentlyClose(key);
            }
        }
    }

    /**
     * Register a client to the selector if the ServerSocketChannel accepts someone
     * @param key the key that trigger a doAccept
     * @throws IOException if system error
     */
    private void doAccept(SelectionKey key) throws IOException {
        ServerSocketChannel ssc = (ServerSocketChannel) key.channel();
        SocketChannel client = ssc.accept();
        if (client == null)
            return;
        client.configureBlocking(false);
        key = client.register(selector, SelectionKey.OP_READ);
        key.attach(new Context(this, key));
    }


    /**
     * silently close a Channel (Server/Client)
     * If it's a client, will be also removed from Database
     * @param key the Key to close
     */
    private void silentlyClose(SelectionKey key) {
        Channel sc = key.channel();
        Context client = (Context) key.attachment();
        try {
            sc.close();
            if (client != null && client.login != null) {
                deleteClient(client.login.getStr());
            }
        } catch (IOException e) {
            // ignore exception
        }
    }


    public static void main(String[] args) throws NumberFormatException, IOException {
        if (args.length != 1) {
            usage();
            return;
        }
        new Server(Integer.parseInt(args[0])).launch();
    }

    private static void usage() {
        System.out.println("Usage : Server port");
    }

    /**
     * Theses methods are here to help understanding the behavior of the selector
     * @param key the selected key
     * @return {@code String} of interestOps
     */
    private String interestOpsToString(SelectionKey key) {
        if (!key.isValid()) {
            return "CANCELLED";
        }
        int interestOps = key.interestOps();
        ArrayList<String> list = new ArrayList<>();
        if ((interestOps & SelectionKey.OP_ACCEPT) != 0) list.add("OP_ACCEPT");
        if ((interestOps & SelectionKey.OP_READ) != 0) list.add("OP_READ");
        if ((interestOps & SelectionKey.OP_WRITE) != 0) list.add("OP_WRITE");
        return String.join("|", list);
    }

    private void printKeys() {
        Set<SelectionKey> selectionKeySet = selector.keys();
        if (selectionKeySet.isEmpty()) {
            System.out.println("The selector contains no key : this should not happen!");
            return;
        }
        System.out.println("The selector contains:");
        for (SelectionKey key : selectionKeySet) {
            SelectableChannel channel = key.channel();
            if (channel instanceof ServerSocketChannel) {
                System.out.println("\tKey for ServerSocketChannel : " + interestOpsToString(key));
            } else {
                SocketChannel sc = (SocketChannel) channel;
                System.out.println("\tKey for Client " + remoteAddressToString(sc) + " : " + interestOpsToString(key));
            }


        }
    }

    private String remoteAddressToString(SocketChannel sc) {
        try {
            return sc.getRemoteAddress().toString();
        } catch (IOException e) {
            return "???";
        }
    }

    private void printSelectedKey() {
        if (selectedKeys.isEmpty()) {
            System.out.println("There were not selected keys.");
            return;
        }
        System.out.println("The selected keys are :");
        for (SelectionKey key : selectedKeys) {
            SelectableChannel channel = key.channel();
            if (channel instanceof ServerSocketChannel) {
                System.out.println("\tServerSocketChannel can perform : " + possibleActionsToString(key));
            } else {
                SocketChannel sc = (SocketChannel) channel;
                System.out.println("\tClient " + remoteAddressToString(sc) + " can perform : " + possibleActionsToString(key));
            }

        }
    }

    private String possibleActionsToString(SelectionKey key) {
        if (!key.isValid()) {
            return "CANCELLED";
        }
        ArrayList<String> list = new ArrayList<>();
        if (key.isAcceptable()) list.add("ACCEPT");
        if (key.isReadable()) list.add("READ");
        if (key.isWritable()) list.add("WRITE");
        return String.join(" and ", list);
    }
}
