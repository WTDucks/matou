package fr.upem.java.tcp.matou;

import java.util.Objects;

class FileRequestData {
    private final StringData filename;
    private final long filesize;

    FileRequestData(StringData filename, long filesize) {
        Objects.requireNonNull(filename);
        if (filesize < 0) {
            throw new IllegalArgumentException("filesize negative");
        }
        this.filename = filename;
        this.filesize = filesize;
    }

    long getFilesize() {
        return filesize;
    }

    StringData getFilename() {
        return filename;
    }
}
