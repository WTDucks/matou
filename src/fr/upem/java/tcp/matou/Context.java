package fr.upem.java.tcp.matou;

import java.io.IOException;

public interface Context {
    void doRead() throws IOException;
    void doWrite() throws IOException;
    void updateInterestOps();
}
