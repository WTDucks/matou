# matou

2018 TCP Networking Project

Par Guillaume Da Silva et Sivajana Balakuganantham

###Compilation

```
% ant main
% ant jar
% ant docs
```

###Utilisation

####Server

```
% java -jar Server.jar port
```

####Client

```
% java -jar Client.jar username server_ipadress port
```

###A Propos

Les sources (.java) sont dans src/fr.upem.java.tcp.matou
elles sont aussi disponible sur https://bitbucket.org/WTDucks/matou

Des jars deja préparés sont disponible dans "RTU"\
Il y a normalement 2 jar à utiliser, Server.jar et Client.jar, 
respectivement le serveur et un client.

la documentation javadoc est situé dans "javadoc"

les manuels utilisateur et developpeur ainsi que le RFC sont dans "docs"
